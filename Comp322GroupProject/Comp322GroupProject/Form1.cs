﻿/*
 * Authors: Chris Ball, Matt Greathouse, and Joshua Belsterling.
 * Course: COMP 322, Object-Oriented and Advanced Programming
 * Date: December 11th, 2017
 * Description: Bitmap Steganography in C# and .NET: Full implementation.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comp322GroupProject {
    public partial class Form1 : Form {

        //Member Variables:
        private int charsTotal = 0;
        private int charsLeft = 0;
        private int bits = 1;
        private Bitmap bitmap;
        private string openFileName;
        public Form1() {
            InitializeComponent();
        }

        //DONE
        //Calculates the number of characters left
        private void txtMessage_TextChanged(object sender, EventArgs e) {
            charsLeft = charsTotal - txtMessage.Text.Length;
            lblCharsLeft.Text = "Characters Remaining: " + (charsLeft) + "/" + charsTotal;

            //If the message is too big to save:
            if (charsLeft < 0) {
                btnWrite.Enabled = false;
            }
            //Otherwise:
            else {
                btnWrite.Enabled = true;
            }
        }

        //returns the bit "n" of the specified byte
        private int getNthBit(byte b, int n) {
            return ((b >> n) & 0x01);
        }

        //returns the last n bits of the byte
        private int getLastBits(byte b, int n) {
            int lastBits = 0;
            for (int i = 0; i < n; i++) {
                lastBits = (lastBits << 1) | getNthBit(b, i);
            }
            return lastBits;
        }

        private void btnRead_Click(object sender, EventArgs e) {
            int numOfHeaders = 0;
            //Read the message from the image.
            //variable to store the compiled bits into a byte
            byte character = 0;
            int lastBits = 0;
            //buffer to store each byte of our message
            byte[] buf = new byte[bitmap.Width * bitmap.Height * 3];
            int bufferIndex = 0;
            int charIndex = 1;
            for (int y = 0; y < bitmap.Height; y++) {
                for (int x = 0; x < bitmap.Width; x++) {
                    //get the pixel
                    Color pixel = bitmap.GetPixel(x, y);

                    //loops three times, one for each byte in the pixel
                    for (int z = 0; z < 3; z++) {
                        switch (z) {
                            //get the last bits of the pixel
                            case 0:
                                lastBits = getLastBits(pixel.R, bits);
                                break;
                            case 1:
                                lastBits = getLastBits(pixel.G, bits);
                                break;
                            case 2:
                                lastBits = getLastBits(pixel.B, bits);
                                break;
                        }

                        //append to the character variable "bits" times
                        for (int b = 0; b < bits; b++) {
                            //append the last bits of the pixel to the MSB of character and shift it right
                            character = Convert.ToByte((character >> 1) | (getNthBit((byte)lastBits, b)) << 7);

                            //once we have enough bits to complete the character, write it to the buffer
                            if ((charIndex) % 8 == 0 && character != '\0') {
                                //once we have enough bits to complete a character, add the character to buf
                                buf[bufferIndex] = character;

                                //Detects header values
                                char temp = (char)character;
                                if (temp == '`') {
                                    numOfHeaders++;
                                }

                                character = 0;
                                bufferIndex++;

                                //If the header is read in, continue; otherwise, abort mission
                                if (charIndex == 16 && numOfHeaders != 2) {
                                    txtMessage.Text = "";

                                    //Resets numOfHeaders and then returns
                                    numOfHeaders = 0;
                                    return;
                                }
                            }
                            else if ((charIndex) % 8 == 0 && character == '\0') {
                                string rtnValue = Encoding.ASCII.GetString(buf);
                                rtnValue = rtnValue.Remove(0, 2);
                                txtMessage.Text = rtnValue;
                                return;

                            }
                            charIndex++;
                        }
                    }
                }
            }
            //convert the bytes in buf to ascii and put it in the text box
            //Turns the returned value into a string, removes the header, and then returns
            string returnValue = Encoding.ASCII.GetString(buf);
            returnValue = returnValue.Remove(0, 2);
            txtMessage.Text = returnValue;

            //Also resets number of headers for the next time
            numOfHeaders = 0;
        }

        private void btnWrite_Click(object sender, EventArgs e) {
            //Write the message to the image.
            string message = "``" + txtMessage.Text;
            message += '\0';
            message += '\0';
            int i = 0, x = 0, y = 0;

            //clear the last bits of the pixel
            
            /*bool broke = false;
            for (y = 0; y < bitmap.Height; y++)
            {
                for (x = 0; x < bitmap.Width; x++)
                {
                    Color currentPixel = bitmap.GetPixel(x, y);
                    bitmap.SetPixel(x, y, Color.FromArgb(currentPixel.A, (currentPixel.R & (~((1 << bits) - 1))),
                        (currentPixel.G & (~((1 << bits) - 1))), (currentPixel.B & (~((1 << bits) - 1)))));
                    if(y*bitmap.Width + x > (message.Length*bits*8)+2) {
                        broke = true;
                        break;
                    } 
                }
                if(broke) {
                    break;
                }
            }
            */
            x = 0;
            y = 0;

            //byte to write to the bmp
            byte byteToWrite = 0;
            int charIndex = 0;
            foreach (char character in message) {
                //for every byte in the character of the message
                for (int j = 0; j < 8; j++) {
                    if (x == bitmap.Width) {
                        y++;
                        x = 0;
                    }
                    if (y >= bitmap.Height) {
                        break;
                    }

                    //color stores the pixel value
                    Color color = bitmap.GetPixel(x, y);

                    //shift the byte we want to write by one, then add a bit from the message to the end of it.
                    byteToWrite = Convert.ToByte((byteToWrite << 1) | getNthBit((byte)character, j));

                    //once the byte ready to be written to a pixel, write it to r, g, or b
                    if (((i + 1) % bits) == 0) {
                        switch ((charIndex) % 3) {
                            case 0:
                                //add the byte to the color and set the pixel on the bitmap
                                //if bits is set to 2, it clears the least significant 2 bits
                                color = Color.FromArgb(255, color.R & (~((1 << bits) - 1)) | byteToWrite, color.G, color.B);
                                bitmap.SetPixel(x, y, color);
                                break;
                            case 1:
                                color = Color.FromArgb(255, color.R, color.G & (~((1 << bits) - 1)) | byteToWrite, color.B);
                                bitmap.SetPixel(x, y, color);
                                break;
                            case 2:
                                color = Color.FromArgb(255, color.R, color.G, color.B & (~((1 << bits) - 1)) | byteToWrite);
                                bitmap.SetPixel(x, y, color);
                                //we've reached b so move on to the next pixel in bitmap by incrementing x
                                x++;
                                break;
                        }
                        //reset byteToWrite
                        byteToWrite = 0;
                        charIndex++;
                    }
                    i++;
                }
            }
        }

        //DONE
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
            //Get save location and name.
            string fileName = "";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK) {
                fileName = saveFileDialog1.FileName;

                //If not saving to the same file, continue
                if (openFileName != fileName) {
                    //Save the data from the bitmap file here.
                    //Help from https://msdn.microsoft.com/en-us/library/9t4syfhh(v=vs.110).aspx
                    bitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Bmp);
                }

                //Otherwise, copy the memory from the original bitmap to a new bitmap
                else {
                    Bitmap bmp = null;
                    //using is a glorified way of disposing of the bitmap after we don't need it in memory
                    using (bitmap) {
                        bmp = new Bitmap(bitmap);
                    }

                    //save the bmp
                    bmp.Save(fileName, ImageFormat.Bmp);
                    //open the new and improved bitmap :D
                    bitmap = (Bitmap)Bitmap.FromFile(fileName);
                    pictureBox.Image = bitmap;
                }
            }
        }

        //DONE
        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            //Open the bmp file.

            string fileName = "";
            if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                //Obtaining the file name and saving it for testing when saving
                fileName = openFileDialog1.FileName;
                openFileName = fileName;
                //Read in data from the bitmap file here.
                bitmap = (Bitmap)Bitmap.FromFile(fileName);
                pictureBox.Image = bitmap;
                //UI Setup:
                txtMessage.Enabled = true;
                btnRead.Enabled = true;
                btnWrite.Enabled = true;
                saveAsToolStripMenuItem.Enabled = true;
                nudBits.Enabled = true;

                //Updates info
                updateCharsLeft();
            }
        }

        //DONE
        //Updates bits-per-pixel and changes the charsLeft to compensate
        private void nudBits_ValueChanged(object sender, EventArgs e) {
            bits = (int)nudBits.Value;
            updateCharsLeft();
        }

        //DONE
        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        //Auxillary function for writing charsLeft
        private void updateCharsLeft() {
            //Changes/prints the amount of characters left
            //Determines the number of bytes
            charsTotal = bitmap.Width * bitmap.Height * 3;
            //Then determines the number of characters that can be stored
            //based on the number of bits per RGB
            float temp = charsTotal;
            temp *= bits;
            temp /= 8;
            charsTotal = (int)temp;
            charsTotal -= 5; //Leaving room for the header and footer

            //Then sets charsLeft label
            charsLeft = charsTotal - txtMessage.Text.Length;
            lblCharsLeft.Text = "Characters Remaining: " + (charsLeft) + "/" + charsTotal;
        }

        private void txtMessage_MouseDoubleClick(object sender, MouseEventArgs e) {
            //Selects all to make it easier to clear/copy the message.
            txtMessage.SelectAll();
        }
    }
}
